============
Installation
============

At the command line::

    $ easy_install djongo_auth

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv djongo_auth
    $ pip install djongo_auth
