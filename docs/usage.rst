=====
Usage
=====

To use djongo_auth in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'djongo_auth.apps.DjongoAuthConfig',
        ...
    )

Add djongo_auth's URL patterns:

.. code-block:: python

    from djongo_auth import urls as djongo_auth_urls


    urlpatterns = [
        ...
        url(r'^', include(djongo_auth_urls)),
        ...
    ]
