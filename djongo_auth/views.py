# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    UpdateView,
    ListView
)

from .models import (
	User,
	Group,
)


class UserCreateView(CreateView):

    model = User


class UserDeleteView(DeleteView):

    model = User


class UserDetailView(DetailView):

    model = User


class UserUpdateView(UpdateView):

    model = User


class UserListView(ListView):

    model = User


class GroupCreateView(CreateView):

    model = Group


class GroupDeleteView(DeleteView):

    model = Group


class GroupDetailView(DetailView):

    model = Group


class GroupUpdateView(UpdateView):

    model = Group


class GroupListView(ListView):

    model = Group

