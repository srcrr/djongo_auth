# -*- coding: utf-8
from django.apps import AppConfig


class DjongoAuthConfig(AppConfig):
    name = 'djongo_auth'
