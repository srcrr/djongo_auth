=============================
djongo_auth
=============================

.. image:: https://badge.fury.io/py/djongo_auth.svg
    :target: https://badge.fury.io/py/djongo_auth

.. image:: https://travis-ci.org/src-r-r/djongo_auth.svg?branch=master
    :target: https://travis-ci.org/src-r-r/djongo_auth

.. image:: https://codecov.io/gh/src-r-r/djongo_auth/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/src-r-r/djongo_auth

Authentication models for the djongo package.

Documentation
-------------

The full documentation is at https://djongo_auth.readthedocs.io.

Quickstart
----------

Install djongo_auth::

    pip install djongo_auth

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'djongo_auth.apps.DjongoAuthConfig',
        ...
    )

Add djongo_auth's URL patterns:

.. code-block:: python

    from djongo_auth import urls as djongo_auth_urls


    urlpatterns = [
        ...
        url(r'^', include(djongo_auth_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
